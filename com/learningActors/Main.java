package com.learningActors;

import com.learningActors.arrays.*;
import com.learningActors.strings.*;

public class Main {
    public static void main(String[] args) {

        ReverseString reverseString = new ReverseString();
        String s1 = "Repaper";
        reverseString.printReverse(s1);
        System.out.println();

        PalindromeString readBackwards = new PalindromeString();
        readBackwards.printPalindrome(s1);
        System.out.println();

        HammingDistance hammingDistance = new HammingDistance();
        String s2 = "Recipes";
        hammingDistance.printHammingDist(s1, s2);
        System.out.println();

        WordFrequency wordFrequency = new WordFrequency();
        String s3 = "Anna";
        String s4 = "Anna has a friend who's name is also Anna";
        wordFrequency.printWordFrequency(s3, s4);
        System.out.println();

        TrimSpaces trimSpaces = new TrimSpaces();
        String s5 = "Hello       Java     World";
        trimSpaces.printTrimSpaces(s5);
        System.out.println();

        ReplaceSubstring replaceSubstring = new ReplaceSubstring();
        String s6 = "TestForSubstring";
        String s7 = "123";
        int position = 4;
        replaceSubstring.printReplaceSubstring(s6, position, s7);
        System.out.println();

        BinaryToDecimal binaryToDecimal = new BinaryToDecimal();
        String s8 = "1011110000";
        binaryToDecimal.printBin2Dec(s8);
        System.out.println();

        ValidatePassword password = new ValidatePassword();
        String s9 = "1P@ssword";
        password.validatePassword(s9);
        System.out.println();

        CalculateSum calculateSum = new CalculateSum();
        int[] ar1 = {2, 5, 18, 20, 100, 54, 69};
        calculateSum.printSum(ar1);
        System.out.println();

        StoreValues storeToArray = new StoreValues();
        int start = 1;
        int end = 100;
        storeToArray.printArray(start, end);
        System.out.println();

        FindMinMax findMinMax = new FindMinMax();
        findMinMax.printMinMaX(ar1);
        System.out.println();

        PrintCommonElements printCommonElements = new PrintCommonElements();
        int[] ar2 = {2, 14, 52, 100, 34, 5};
        printCommonElements.printCommons(ar1, ar2);
        System.out.println();

        PrintAveragePercentage printAveragePercentage = new PrintAveragePercentage();
        printAveragePercentage.printSumAndAverage(ar2);
        printAveragePercentage.printPercentage(ar2);
        System.out.println();

        PrintSumCapitals printSumCapitals = new PrintSumCapitals();
        String[] str1 = {"Apple", "Banana", "Pear", "orange"};
        printSumCapitals.printHowManyCapital(str1);
        System.out.println();

        BubbleSort bubbleSort = new BubbleSort();
        bubbleSort.printSortedArray(ar2);
        System.out.println();

        int[][] arrD = {{1, 2, 3, 4}, {5, 6, 7, 8}, {9, 10, 11, 12}};
        PrintMainDiagonal printMainDiagonal = new PrintMainDiagonal();
        printMainDiagonal.printMainDiag(arrD);
        System.out.println();

        PrintSumOfColumns printSumOfColumns = new PrintSumOfColumns();
        printSumOfColumns.printSumColumn(arrD);
        System.out.println();

        SplitArray splitArray = new SplitArray();
        int[] ar3 = {2, 14, 52, 100, 34, 5, 4, 55, 42, 15};
        int p = 34;
        splitArray.Split(ar3, p);
    }
}
