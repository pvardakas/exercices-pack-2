package com.learningActors.strings;

public class ReverseString {

    static String reverseString(String string) {
        char[] reverse = new char[string.length()];

        int i = 0;
        int j = string.length() - 1;

        while (i < reverse.length) {
            while (j >= 0) {
                reverse[i] = string.charAt(j);
                i++;
                j--;
            }
        }
        return new String(reverse);
    }

    public void printReverse(String string) {
        System.out.println("String normal: " + string);
        System.out.println("String reversed: " + reverseString(string));
    }
}
