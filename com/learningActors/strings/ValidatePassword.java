package com.learningActors.strings;

public class ValidatePassword {

    public void validatePassword(String password) {
        int length = password.length();

        if (length < 8) {
            System.out.println("Invalid Password");
        } else {
            int criteria = 1;
            boolean hasNumber = false;
            boolean hasSpChar = false;
            boolean hasSequence = false;
            boolean hasSequenceCons = false;

            if (!password.equals(password.toLowerCase())) {
                criteria++;
            }

            if (!password.equals(password.toUpperCase())) {
                criteria++;
            }

            char[] chars = password.toCharArray();
            char[] specialChars = {'!', '@', '[', ']', '#', '$', '%', '^', '&', '*', '{', '}', '~', '(', ')'};

            for (int i = 2; i < chars.length; i++) {

                if (!hasNumber) {
                    if (Character.isDigit(password.charAt(i - 2))) {
                        hasNumber = true;
                        criteria++;
                    }
                }

                if (!hasSpChar) {
                    for (char spc : specialChars) {
                        if (password.charAt(i - 2) == spc) {
                            hasSpChar = true;
                            criteria++;
                            break;
                        }
                    }
                }

                if (!hasSequence) {
                    if (password.charAt(i - 2) == password.charAt(i) && password.charAt(i - 1) == password.charAt(i)) {
                        hasSequence = true;

                    }
                }

                if (!hasSequenceCons) {
                    if (password.charAt(i - 2) - password.charAt(i - 1) == -1 && password.charAt(i - 1) - password.charAt(i) == -1) {
                        hasSequenceCons = true;
                    }
                }
            }

            if (!hasSequence && !hasSequenceCons) {
                criteria++;
            }

            if (criteria < 3) {
                System.out.println("Invalid Password!");
            } else if (criteria == 3 || criteria == 4) {
                System.out.println("Password OK!");
            } else if (criteria == 5) {
                System.out.println("Strong password!");
            } else {
                System.out.println("Very Strong password!");
            }
        }
    }
}