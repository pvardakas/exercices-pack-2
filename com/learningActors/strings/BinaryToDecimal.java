package com.learningActors.strings;

public class BinaryToDecimal {

    private int binary2Decimal(String binary) {
        char[] chars = binary.toCharArray();
        int dec = 0;
        double pos = chars.length;
        for (int i = 0; i < chars.length; i++) {
            if (binary.charAt(i) == '1') {
                double pow = Math.pow(2, (pos - 1));
                dec += 1 * pow;
                pos--;
                continue;
            }
            pos--;
        }
        return dec;
    }

    public void printBin2Dec(String binary) {
        System.out.println("Binary = " + binary + " converted to decimal: " + binary2Decimal(binary));
    }
}
