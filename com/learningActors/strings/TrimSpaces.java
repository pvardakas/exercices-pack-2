package com.learningActors.strings;

public class TrimSpaces {

    private String trimSpaces(String text) {
        return text.trim().replaceAll(" +", " ");
    }

    public void printTrimSpaces(String text) {
        String textTrim = trimSpaces(text);
        System.out.println("Text with spaces: '" + text + "' " + "becomes: '" + textTrim + "'");
    }
}
