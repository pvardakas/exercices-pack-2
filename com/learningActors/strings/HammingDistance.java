package com.learningActors.strings;

public class HammingDistance {

    private int hammingDistance(String strA, String strB) {
        int distance = 0;
        int i = 0;
        int j = 0;

        if (strA.length() != strB.length()) {
            return distance;
        } else {
            while (i < strA.length()) {
                while (j < strB.length()) {
                    if (strA.charAt(i) != strB.charAt(j)) {
                        distance++;
                        i++;
                        j++;
                        break;
                    }
                    i++;
                    j++;
                    break;
                }
            }
            return distance;
        }
    }

    public void printHammingDist(String strA, String strB) {
        System.out.println("The Hamming Distance of words: '" + strA + " & " + strB + "' is: " + hammingDistance(strA, strB));
    }
}
