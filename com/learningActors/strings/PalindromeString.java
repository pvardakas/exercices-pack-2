package com.learningActors.strings;

public class PalindromeString {

    private boolean palindromeReverse(String string) {
        String reversed = ReverseString.reverseString(string);
        return string.equalsIgnoreCase(reversed);
    }

    private boolean palindromeEquals(String string) {
        String s = string.toLowerCase();
        int i = 0;
        int j = (s.length() - 1 - i);

        while (i < j) {
            if (s.charAt(i) != s.charAt(j)) {
                return false;
            }
            i++;
            j--;
        }
        return true;
    }

    public void printPalindrome(String string) {
        if (palindromeReverse(string)) {
            System.out.println("The word '" + string + "' can be read backwards (Reverse Method)");
        } else {
            System.out.println("The word '" + string + "' can't be read backwards (Reverse Method)");
        }

        if (palindromeEquals(string)) {
            System.out.println("The word '" + string + "' can be read backwards (Equal Method)");
        } else {
            System.out.println("The word '" + string + "' can't be read backwards (Equal Method)");
        }
    }
}
