package com.learningActors.strings;

public class WordFrequency {

    private double wordFrequency(String word, String text) {
        String[] tokens = text.trim().split(" ");
        int count = 0;
        double frequency;
        boolean hasOnlyWSpeces = false;
        for (String t : tokens) {
            if (t.equals("")) {
                hasOnlyWSpeces = true;
                break;
            }
        }
        if (hasOnlyWSpeces) {
            return 0;
        } else {
            for (String str : tokens) {
                if (str.equalsIgnoreCase(word)) {
                    count++;
                }
            }
            frequency = (double) count / tokens.length;
            return frequency;
        }
    }

    public void printWordFrequency(String word, String text) {
        System.out.println("The frequency of the word: '" + word + "' in text '" + text + "' is: " + wordFrequency(word, text));
    }
}
