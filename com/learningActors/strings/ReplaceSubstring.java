package com.learningActors.strings;

public class ReplaceSubstring {

    private String replaceSubstring(String string, int position, String substring) {
        int strLength = string.length();
        int subStrLength = substring.length();
        if (position > (strLength - subStrLength - 1)) {
            return null;
        } else {
            String s = string.substring(position, position + subStrLength);
            return string.replaceAll(s, substring);
        }
    }

    public void printReplaceSubstring(String string, int position, String substring) {
        String replacedSubStr = replaceSubstring(string, position, substring);
        if (replacedSubStr == null) {
            System.out.println("Error! Can't replace substring on given string");
        } else {
            System.out.println("Given string: " + string + " becomes after insertion: " + replacedSubStr);
        }
    }
}
