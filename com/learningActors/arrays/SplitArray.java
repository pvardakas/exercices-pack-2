package com.learningActors.arrays;

public class SplitArray {

    public void Split(int[] arr, int value) {
        Integer position = null;
        int length = arr.length;
        BubbleSort.bubbleSortArray(arr);

        for (int i = 0; i < arr.length; i++) {
            if (arr[i] == value) {
                position = i;
                break;
            }
        }
        if (position == null) {
            System.out.print("Array can't split because element not found");
        } else {
            if (position > 0 && position < arr.length - 1) {
                int l1 = position + 1;
                int[] arr1 = new int[l1];
                int l2 = length - l1;
                int[] arr2 = new int[l2];

                System.out.print("Splitted Array 1: ");
                for (int i = 0; i < arr1.length; i++) {
                    arr1[i] = arr[i];
                    System.out.print(arr1[i] + "\t");
                }
                System.out.println();
                System.out.print("Splitted Array 2: ");

                for (int i = 0; i < arr2.length; i++) {
                    arr2[i] = arr[l1 + i];
                    System.out.print(arr2[i] + "\t");
                }
                System.out.println();
            } else {
                if (position == 0) {
                    System.out.print("Array can't split because element found at first position");
                }
                if (position == arr.length - 1) {
                    System.out.print("Array can't split because element found at last position");
                }
            }
        }
    }
}


