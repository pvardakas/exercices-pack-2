package com.learningActors.arrays;

public class FindMinMax {

    private int[] findMinMax(int[] array) {
        int min, max;
        min = max = array[0];

        for (int i = 1; i < array.length; i++) {
            if (array[i] < min) {
                min = array[i];
            }
            if (array[i] > max) {
                max = array[i];
            }
        }
        return new int[]{min, max};
    }

    public void printMinMaX(int[] array) {
        int[] minMax = findMinMax(array);
        System.out.print("Printing Min and Max of Array: ");
        for (int n : minMax) {
            System.out.print(n + "\t");
        }
        System.out.println();
    }
}
