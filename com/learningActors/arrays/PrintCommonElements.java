package com.learningActors.arrays;

public class PrintCommonElements {

    public void printCommons(int[] ar1, int[] ar2) {

        System.out.print("Printing Commons of Arrays: ");
        for (int a : ar1) {
            for (int b : ar2) {
                if (a == b) {
                    System.out.print(a + "\t");
                }
            }
        }
        System.out.println();
    }
}
