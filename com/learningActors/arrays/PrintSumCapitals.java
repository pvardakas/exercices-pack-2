package com.learningActors.arrays;

public class PrintSumCapitals {

    public void printHowManyCapital(String[] strings) {
        int count = 0;
        for (String str : strings) {
            if (Character.isUpperCase(str.charAt(0))) {
                count++;
            }
        }
        System.out.println("The array has " + count + " words that starts with capital letter");
    }
}
