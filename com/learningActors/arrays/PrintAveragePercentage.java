package com.learningActors.arrays;

public class PrintAveragePercentage {

    public void printSumAndAverage(int[] array) {
        int sum = CalculateSum.calculateSum(array);
        int length = array.length;
        System.out.println("The Sum of the Array is " + sum + " and the Average is: " + sum / length);
    }

    private double calculatePercentage(int[] array) {
        int sum = CalculateSum.calculateSum(array);
        int length = array.length;
        double average = (double) sum / length;
        int count = 0;

        for (int a : array) {
            if (a < average) {
                count++;
            }
        }
        return ((double) count / length) * 100;
    }

    public void printPercentage(int[] arr1) {
        double percentage = calculatePercentage(arr1);
        System.out.println("Percentage of elements below average = " + (double) Math.round(percentage * 100d) / 100d + "%");
    }
}
