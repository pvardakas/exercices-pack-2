package com.learningActors.arrays;

public class StoreValues {

    private int[] storeToArray(int start, int end) {
        int[] array = new int[end];

        for (int i = start - 1; i < array.length; i++) {
            array[i] = i + 1;
        }
        return array;
    }

    public void printArray(int start, int end) {
        System.out.println("Array that contains values from 1 - 100");
        int[] array = storeToArray(start, end);
        for (int num : array) {
            if (num % 20 == 0) {
                System.out.println(num);
            } else {
                System.out.print(num + "\t");
            }
        }
    }
}
