package com.learningActors.arrays;

public class PrintSumOfColumns {

    public void printSumColumn(int[][] arrD) {
        int rows = arrD.length;
        int columns = arrD[0].length;
        int sum;

        int i = 0;
        System.out.println("Printing the Sum of Columns: ");
        while (i < columns) {
            sum = 0;
            for (int j = 0; j < rows; j++) {
                sum += arrD[j][i];
            }
            System.out.println("Column: " + i + " Sum: " + sum);
            i++;
        }
    }
}
