package com.learningActors.arrays;

public class BubbleSort {

    static int[] bubbleSortArray(int[] arr) {
        for (int i = 0; i < arr.length; i++) {
            for (int j = 1; j < arr.length - i; j++) {
                if (arr[j - 1] > arr[j]) {
                    int temp = arr[j - 1];
                    arr[j - 1] = arr[j];
                    arr[j] = temp;
                }
            }
        }
        return arr;
    }

    public void printSortedArray(int[] arr) {
        System.out.println("Printing Unsorted array: ");
        for (int i : arr) {
            System.out.print(i + "\t");
        }
        System.out.println();

        int[] sortedArr = bubbleSortArray(arr);
        System.out.println("Printing Sorted array: ");
        for (int s : sortedArr) {
            System.out.print(s + "\t");
        }
        System.out.println();
    }
}
