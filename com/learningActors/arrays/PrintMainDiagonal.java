package com.learningActors.arrays;

public class PrintMainDiagonal {

    public void printMainDiag(int[][] arrD) {
        int rows = arrD.length;
        int i = 0;
        System.out.println("Printing the elements of main diagonal of array: ");

        while (i < rows) {
            System.out.print(arrD[i][i] + "\t");
            i++;
        }
        System.out.println();
    }
}
