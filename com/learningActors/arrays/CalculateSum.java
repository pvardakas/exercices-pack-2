package com.learningActors.arrays;

public class CalculateSum {

    static int calculateSum(int[] array) {
        int sum = 0;
        for (int num : array) {
            sum += num;
        }
        return sum;
    }

    public void printSum(int[] array) {
        System.out.println("The Sum of the elements of the array = " + calculateSum(array));
    }
}
